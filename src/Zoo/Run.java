package Zoo;
import java.util.List;
import java.util.ArrayList;
/**
 * Created by user on 18.10.2016.
 */
@SuppressWarnings("ALL")
public class Run {
public static void main (String [] args) {
 Shark shark = new Shark(" Karp ");
 Monkey monkey = new Monkey(" Filip ");
 Zebra zebra = new Zebra(" Alex ");
 Bear bear = new Bear(" Ted ");




 ZooBox<Shark> zooBoxShark = new ZooBox<>();
 ZooBox<Monkey> zooBoxMonkey = new ZooBox<>();
 ZooBox<Zebra> zooBoxZebra = new ZooBox<>();
 ZooBox<Bear> zooBoxBear = new ZooBox<>();



 zooBoxShark.lockAnimal(shark);
 zooBoxMonkey.lockAnimal(monkey);
 zooBoxZebra.lockAnimal(zebra);
 zooBoxBear.lockAnimal(bear);



 zooBoxShark.getAnimal().sayName();
 zooBoxMonkey.getAnimal().sayName();
 zooBoxZebra.getAnimal().sayName();
 zooBoxBear.getAnimal().sayName();


 ZooBox<Animal> zooBoxAll = new ZooBox<>();
 zooBoxAll.lockAnimal(shark);
 zooBoxAll.getAnimal().sayName();
 List<ZooBox> zooAll = new ArrayList<>();


 zooAll.add(zooBoxShark);
 zooAll.add(zooBoxMonkey);
 zooAll.add(zooBoxZebra);
 zooAll.add(zooBoxBear);
 zooAll.add(zooBoxAll);



 for (ZooBox zooBox : zooAll){
  if(zooBox.getAnimal() != null) zooBox.getAnimal().sayName(); }
 }

}



