package Zoo;

/**
 * Created by user on 18.10.2016.
 */
public class Bear extends Animal {
    public Bear (String name){
        this.name = name;
        this.animalType = AnimalType.MAMMAL;

    }

    @Override
   public void sayName(){

        System.out.println(" I'm a " + this.animalType + "." + " My name is " + this.name + "." );



    }

}
