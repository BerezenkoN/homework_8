package Sport;
import java.util.List;
/**
 * Created by user on 19.10.2016.
 */
public class Sport <K extends Discipline, V extends Constituent> {
    private K discipline;
    private List<V> constituents;


    public Sport(K discipline, List<V> constituens) {
        this.discipline = discipline;
        this.constituents = constituents;
    }

    public void cantLiveWithout() {

        StringBuilder sb = new StringBuilder();
        sb.append(discipline.description() + " It can't live without ");
        for (V constituent : constituents)
            sb.append(constituent.about());
        System.out.println(sb.toString());


    }
}
