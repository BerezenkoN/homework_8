package Sport;
import java.util.List;
import java.util.ArrayList;


/**
 * Created by user on 21.10.2016.
 */

public class Run {
    public static void main(String[] args) {

        Football football = new Football(" Football ", " Game description ");
        Hockey hockey = new Hockey(" Hockey ", " Game description ");
        Formula formula = new Formula(" Formula1 ", " Game description ");

        Ball ball = new Ball("leather ball");
        Car car = new Car("Fire car");
        Ice ice = new Ice("Hot Ice");
        Fans fans = new Fans("Crazy fans");

        List<Constituent> footballConstituents = new ArrayList<>();
        footballConstituents.add(fans);
        footballConstituents.add(ball);

        Sport <Football, Constituent> footballPassion = new Sport<> (football, footballConstituents);
        footballPassion.cantLiveWithout();

    }

}


